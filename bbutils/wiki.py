import os
import shutil
from .tickets import _trac_to_markdown
from sqlalchemy.ext.automap import automap_base
from .tickets import AttachmentFile
from sqlalchemy import create_engine, func, and_, Column
from sqlalchemy.types import NULLTYPE
from sqlalchemy.orm import Session

def _wiki_to_markdown(text, pagename):
    return _trac_to_markdown(text, pagename)



Base = automap_base()

def _mkdirs(dest):
    dir_, fname = os.path.split(dest)
    try:
        os.makedirs(dir_)
    except OSError as e:
        if e.errno != os.errno.EEXIST:
            raise

class Wiki(Base):
    __tablename__ = 'wiki'

    name = Column(NULLTYPE, primary_key=True)
    version = Column(NULLTYPE, primary_key=True)

    def to_file(self, root):
        if self.name == "WikiStart":
            name = "Home"
        else:
            name = self.name
        text = _wiki_to_markdown(self.text, name)
        dest = os.path.join(
                    root, os.path.normpath(name) + ".md"
                )
        _mkdirs(dest)
        print("Writing %s" % dest)
        with open(dest, "w") as file_:
            file_.write(text.encode('utf-8'))

class Attachment(Base):
    __tablename__ = 'attachment'

    type = Column(NULLTYPE, primary_key=True)
    id = Column(NULLTYPE, primary_key=True)
    filename = Column(NULLTYPE, primary_key=True)

    def to_file(self, attachment_path, root):
        af = AttachmentFile("wiki", self.filename, self.id)
        dest = os.path.join(root, os.path.normpath(self.id), self.filename)
        _mkdirs(dest)
        src = af.source_file(attachment_path)
        print("Copying %s to %s" % (src, dest))
        shutil.copy(src, dest)

def trac_to_wiki(url, attachments, output):
    output = output or "./wiki"

    engine = create_engine(url)

    Base.prepare(engine, reflect=True)

    session = Session(engine, autoflush=False)

    max_wiki = session.query(Wiki.name, func.max(Wiki.time).label('maxtime')).group_by(Wiki.name).subquery()
    for wiki in session.query(Wiki).filter(Wiki.author != 'trac').join(max_wiki,
                                and_(Wiki.name == max_wiki.c.name,
                                        Wiki.time == max_wiki.c.maxtime)):
        wiki.to_file(output)

    for attachment in session.query(Attachment).filter_by(type='wiki'):
        attachment.to_file(attachments, output)

