import requests
from requests.auth import HTTPBasicAuth
import posixpath
from lxml import etree
from StringIO import StringIO
import json
import time


class BitBucket(object):
    def __init__(self, username, email, password):
        self.base_url = "https://bitbucket.org/"
        self.username = username
        self.email = email
        self.password = password

    log_requests = True

    def _with_retries(self, fn, *arg, **kw):
        for retry in range(3):
            try:
                ret = fn(*arg, **kw)
            except IOError as err:
                self._output(
                    "Got error %s, sleeping for 5 then "
                    "retrying for %d" % (err, retry))
                time.sleep(5)
            else:
                return ret

    def _get(self, path, headers=None):
        url = self._url(path)
        if self.log_requests:
            self._output("GET %s" % url)
        return self._with_retries(
            requests.get,
            url, headers=headers,
            auth=HTTPBasicAuth(self.email, self.password)
        )

    def _put(self, path, data=None, headers=None):
        url = self._url(path)
        if self.log_requests:
            self._output("PUT %s" % url)
        return self._with_retries(
            requests.put,
            url, data=data,
            headers=headers,
            auth=HTTPBasicAuth(self.email, self.password)
        )

    def _post(self, path, data=None, headers=None):
        url = self._url(path)
        if self.log_requests:
            self._output("POST %s" % url)
        return self._with_retries(
            requests.post,
            url, data=data,
            headers=headers,
            auth=HTTPBasicAuth(self.email, self.password)
        )

    def _url(self, path):
        if not path.startswith("/"):
            raise ValueError("relative urls aren't supported...")

        dirname, file_ = posixpath.split(path)
        self.relative_to = dirname

        if path.startswith("/"):
            path = path[1:]
        return posixpath.join(self.base_url, path)

    def _output(self, msg):
        if isinstance(msg, unicode):
            msg = msg.encode('ascii', errors='ignore')
        print(msg)

    def _paged_iterator(self, uri):
        start = 0
        while True:
            to_get = uri + "?start=%d&limit=50" % start
            response = self._get(to_get)
            struct = json.loads(response.text)
            yield struct

            start += 50

    def issues(self, repo):
        for struct in self._paged_iterator(
            "/api/1.0/repositories/%s/%s/issues" %
                (self.username, repo)
        ):
            issues = struct["issues"]
            if not issues:
                break
            for issue in issues:
                yield issue

    def comments(self, repo, issue):
        response = self._get(
            "/api/1.0/repositories/%s/%s/issues/%s/comments" %
            (self.username, repo, issue))
        struct = json.loads(response.text)
        for comment in struct:
            yield comment

    def login(self):
        pass


class WebLoginBitBucket(BitBucket):

    use_referrer = True

    def __init__(self, username, email, password):
        super(WebLoginBitBucket, self).__init__(username, email, password)
        self.referrer = None
        self.session = requests.Session()
        self.use_csrf = False

    def _parse_html(self, response):
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(response.text), parser)
        return tree

    def _get(self, path, headers=None):
        get_headers = {}
        if self.referrer:
            get_headers['referer'] = self.referrer
        if headers:
            get_headers.update(headers)

        url = self.referrer = self._url(path)
        return self._with_retries(
            self.session.get,
            url, headers=headers
        )

    def _post(self, path, data=None, headers=None):
        post_headers = {"referer": self.referrer}
        if headers:
            post_headers.update(headers)
        return self._with_retries(
            self.session.post,
            self._url(path), data=data,
            headers=post_headers
        )

    def login(self):
        username, password, email = self.username, self.password, self.email
        self._output(
            "logging in username '%s' w/ email '%s'" % (username, email))
        response = self._get("/account/signin/?next=/")
        tree = self._parse_html(response)
        loginform = tree.getroot().xpath(
            "//form[@action='/account/signin/']")[0]

        action = loginform.attrib['action']

        params = {"username": email, "password": password}

        # sometimes this is there, sometimes it's not.
        # not sure if BB is turning knobs on their end and maybe looking
        # at browser strings
        csrf = loginform.xpath(
            "//input[@type='hidden'][@name='csrfmiddlewaretoken']"
        )
        if csrf:
            csrf = csrf[0].attrib['value']
            params['csrfmiddlewaretoken'] = csrf
            self.use_csrf = True

        params["next"] = "/"
        response = self._post(action, data=params)
        tree = self._parse_html(response)
        title = tree.getroot().xpath("/html/head/title")[0]
        if self.username not in title.text:
            raise Exception("Based on the <title> tag of %s, "
                            "we think login failed." % title.text)
        self._output("Success: %s" % title.text)
