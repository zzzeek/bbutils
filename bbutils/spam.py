from multiprocessing import Pool
from .client import BitBucket, WebLoginBitBucket

web = None
project = None
def _despam_issue(issue):
    api = BitBucket(web.username, web.password)

    if issue['is_spam']:
        toggle_issue_spam(web, project, issue['local_id'])
    spam_comment_ids = [
        comment['comment_id'] for comment in
        api.comments(project, issue['local_id'])
        if comment['is_spam']
    ]
    if spam_comment_ids:
        toggle_issue_spam(web, project, issue['local_id'], spam_comment_ids)

def _set_issue_params(webclient, projectname):
    global web, project
    web = webclient
    project = projectname

def unmark_spam(username, password, project):
    api = BitBucket(username, password)
    web = WebLoginBitBucket(username, password)
    web.login()
    pool = Pool(10, _set_issue_params, (web, project))

    list(pool.imap(_despam_issue, api.issues(project), chunksize=50))

def toggle_issue_spam(client, repo, issue, comments=None):
    username = client.username

    csrf_form_action = '/%s/%s/issue/spam/%s' % (username, repo, issue)
    params = {}
    if client.use_referrer:
        response = client._get("/%s/%s/issue/%s/" % (username, repo, issue))

        # we're taking advantage of the observation that all the
        # csrfmiddlewaretokens on the page seem to be the same value
        # so we are only fetching it once.
        if client.use_csrf:
            tree = client._parse_html(response)
            spamform = tree.getroot().xpath("//form[@action='%s']" % (csrf_form_action))[0]
            csrf = spamform.xpath(
                    "//input[@type='hidden'][@name='csrfmiddlewaretoken']"
                    )[0].attrib['value']
            params["csrfmiddlewaretoken"] = csrf

    if comments:
        for comment in comments:
            client._output("Approving comment %s on issue %s..." % (comment, issue))
            action = '/%s/%s/issue/spam/%s/%s' % (username, repo, issue, comment)
            response = client._post(action, data=params)
    elif comments is None:
        client._output("Approving issue %s..." % issue)
        action = csrf_form_action
        response = client._post(action, data=params)


