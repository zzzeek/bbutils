import json
import time
import re
from .client import WebLoginBitBucket, BitBucket
import os


class ExportIssuesClient(BitBucket):
    log_requests = False

    def export(self, username, project):
        self._output("Exporting project issues %s/%s" % (username, project))

        response = self._post(
            "/api/1.0/repositories/%s/%s/issues/export" %
            (username, project))
        if response.status_code == 202:
            self._output("seems OK, waiting for job")
        else:
            raise Exception("got status code %s" % response.status_code)
        while True:
            response = self._get(
                "/api/1.0/repositories/%s/%s/issues/export"
                % (username, project))
            struct = json.loads(response.text)
            status = struct['status']
            if status == 'SUCCESS':
                self.download_url = struct['url']
                break
            elif status != 'ACCEPTED':
                raise Exception("unknown status received: %r" % struct)
            else:
                if 'phase' not in struct:
                    continue
                else:
                    self._output(
                        "Processing: %s; %s of %s; %s percent complete" % (
                            struct['phase'],
                            struct['count'],
                            struct['total'],
                            struct['pct']
                        ))
                time.sleep(10)

    def download(self, directory, path):
        response = self._get(self.download_url)

        if path is None:
            try:
                content_disp = response.headers['content-disposition']
            except KeyError:
                raise KeyError(
                    "response has no content-disposition: %r" %
                    response.headers)
            else:
                fname = re.match(r'attachment; filename=(.+)', content_disp)

            if not fname:
                raise Exception(
                    "no attachment name could be parsed: %r" %
                    content_disp)
            else:
                fname = fname.group(1)
        else:
            fname = path

        fname = os.path.join(directory, fname)
        self._output("writing: %s" % fname)
        with open(fname, "wb") as handle:
            handle.write(response.content)
        self._output("complete!")
        return response

