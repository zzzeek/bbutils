# sample trac migration conf.
# supply this to the import_from_trac command using --constants=my_ticket_conf.py

# priority translation map
priorities = {
    None: 'minor',
    '--': 'minor',
    'critical': 'critical',
    'high': 'major',
    'normal': 'minor',
    'low': 'trivial',
    'wish': 'trivial',

    'highest': 'blocker',
    'high': 'critical',
    'medium': 'major',
    'low': 'minor',
    'lowest': 'trivial'
}

# author translation map.
# authors entries not present are passed through.
authors = {
    "anonymous": None,
    "jetbeam": "bearclaw"
}