from setuptools import setup


setup(name='bbutils',
      version=1.0,
      description="bitbucket utilities",
      classifiers=[
      'Development Status :: 4 - Beta',
      'Environment :: Console',
      'Programming Language :: Python',
      'Programming Language :: Python :: Implementation :: CPython',
      ],
      author='Mike Bayer',
      author_email='mike@zzzcomputing.com',
      url='http://bitbucket.org/zzzeek/bbutils',
      license='MIT',
      packages=["bbutils"],
      zip_safe=False,
      install_requires=['requests', 'sqlalchemy>=0.9.2', 'lxml'],
      entry_points={
        'console_scripts': [
            'bbutils = bbutils.cmd:main',
        ],
      }
)
