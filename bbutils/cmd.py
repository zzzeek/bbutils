from argparse import ArgumentParser
import os


def export(options):
    from .export import ExportIssuesClient
    client = ExportIssuesClient(
        options.username, options.email, options.password)
    client.login()
    client.export(options.username, options.project)
    client.download(options.dir, options.output)


def unmark_spam(options):
    from .spam import unmark_spam as _unmark_spam
    _unmark_spam(options.username, options.password, options.project)


def import_from_trac(options):
    from .tickets import trac_to_import
    url = options.url
    output = options.output
    attachments = options.attachments
    opts = {}
    if options.constants:
        dir_, fname = os.path.split(options.constants)
        with open(options.constants) as f:
            code = compile(f.read(), fname, 'exec')
            exec(code, opts)
    trac_to_import(url, attachments, output, opts)


def trac_to_wiki(options):
    from .wiki import trac_to_wiki as _trac_to_wiki
    url = options.url
    output = options.output
    attachments = options.attachments
    _trac_to_wiki(url, attachments, output)


def main(argv=None):
    parser = ArgumentParser(prog="bbutils")
    parser.add_argument("-u", "--username", help="username")
    parser.add_argument("-e", "--email", help="email (used for auth)")
    parser.add_argument(
        "-p", "--password", help="password, or set BB_PW",
        default=os.environ.get("BB_PW"))
    parser.add_argument(
        "-o", "--output",
        help="output filename or full path for file writing commands")

    subparsers = parser.add_subparsers()

    subparser = subparsers.add_parser("export_issues", help="export issues")
    subparser.add_argument("project", help="project name")
    subparser.add_argument("-d", "--dir", default=".", help="target directory")
    subparser.set_defaults(cmd=export)

    subparser = subparsers.add_parser(
        "unmark_spam", help="mark all issues as approved")
    subparser.add_argument("project", help="project name")
    subparser.set_defaults(cmd=unmark_spam)

    subparser = subparsers.add_parser(
        "trac_to_import", help="create a bitbucket issue import from trac")
    subparser.add_argument("url", help="SQLAlchemy URL to database")
    subparser.add_argument("attachments", help="Path to attachments directory")
    subparser.add_argument(
        "--constants", help="Path to a .py file that has redefined constants")
    subparser.set_defaults(cmd=import_from_trac)

    subparser = subparsers.add_parser(
        "trac_to_wiki", help="create a bitbucket wiki filesystem from trac")
    subparser.add_argument("url", help="SQLAlchemy URL to database")
    subparser.add_argument("attachments", help="Path to attachments directory")
    subparser.set_defaults(cmd=trac_to_wiki)

    options = parser.parse_args(argv)
    if not hasattr(options, "cmd"):
        # see http://bugs.python.org/issue9253, argparse
        # behavior changed incompatibly in py3.3
        parser.error("too few arguments")
    else:
        fn = options.cmd
        fn(options)

if __name__ == '__main__':
    main()
