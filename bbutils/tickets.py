import os
import datetime
import re
import json
import zipfile
import urllib
import hashlib
import itertools
import posixpath

from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session, relationship, object_session
from sqlalchemy import String, Column, case, func, util
from sqlalchemy import create_engine


Base = automap_base()

# BB considers text to be creole, not markdown, before this date,
# so the updated_on field has to be set to this as a min
# see: https://confluence.atlassian.com/pages/viewpage.action?pageId=330796872&focusedCommentId=446136372#comment-446136372
BB_MINIMUM_UPDATED = datetime.datetime(2012, 10, 30)

def _convert_time(tractime, min_date=None):
    dt = datetime.datetime.fromtimestamp(tractime/1000000.0)
    if min_date and dt < min_date:
        dt = min_date
    return dt.isoformat()+"+00:00"

def _trac_to_markdown(text, pagename=None):
    if not text:
        return text

    text = re.sub(r'\r\n', '\n', text)

    header_regex = re.compile(r'^(=+) (.+?) (=+)\s*$')
    bracket = re.compile(r'^\s*{{{')
    close_bracket = re.compile(r'^\s*}}}')

    # convert ticket notation
    # ticket links on a line by themselves
    # need leading space to prevent interpretation as a header
    text = re.sub(r'^\[ticket:(\d+)\]', r" #\1", text, flags=re.M)
    text = re.sub(r'\[ticket:(\d+)\]', r"#\1", text)

    def conv_link(m):
        link = m.group(1)
        if link.startswith("wiki:"):
            is_wiki = True
            link = link[5:]
            link = re.sub(r'^"|"$', '', link)
        else:
            is_wiki = False
        description = m.group(2) or link

        if is_wiki:
            if pagename:
                prefix = posixpath.commonprefix([pagename, link])
                if prefix.endswith("/"):
                    link = link[len(prefix):]

            assert not link.startswith("/")

        return "[%s](%s)" % (
                    description,
                    link
                )

    # process across lines within blocks that aren't
    # literal blocks
    in_bracket = False
    newtext = ""
    for block in re.split(r'({{{|}}})', text):
        if block == '{{{':
            in_bracket = True
        elif block == '}}}':
            in_bracket = False
        elif not in_bracket:
            # convert wiki and hyperlinks
            block = re.sub(r'\[([^ ]+?)(?: (.+?))?\]', conv_link, block, flags=re.S)

            # convert '''emphasis'''
            block = re.sub(r"'''(.+?)'''", r'**\1**', block, flags=re.S)

            # convert "!" names
            block = re.sub(r'\!([A-Z][a-zA-Z0-9]+)', r'\1', block)

            # convert r<XYZ> to <XYZ>
            block = re.sub(r'r([a-f0-9]{12,32})', r"\1", block)

        newtext += block

    text = newtext

    # do line-level formatting, headers and code blocks
    indent = 0
    out = []
    for line in text.split('\n'):
        header = header_regex.match(line)
        if header:
            out.append("%s %s" % (
                    "#" * len(header.group(1)),
                    header.group(2)
                ))
        elif bracket.match(line) and indent == 0:
            indent += 4
            out.append("")
        elif close_bracket.match(line) and indent > 0:
            indent -= 4
            out.append("")
        else:
            out.append("%s%s" % (" " * indent, line))
    return "\n".join(out)

default_opts = dict(
    types={
        'defect': 'bug',
        'enhancement': 'enhancement',
        'discussion': 'proposal',
        'note': 'proposal',
        'proposal': 'proposal',
        'task': 'task',
    },
    statuses={
        'new': 'new',
        'accepted': 'open',
        'assigned': 'open',
        'pending': 'open',
        'reopened': 'open',
        'closed': 'resolved',
        'worksforme': 'wontfix',
        'invalid': 'invalid',
        'duplicate': 'duplicate',
        'wontfix': 'wontfix',
        'fixed': 'resolved',
    },
    priorities={
        None: 'minor',
        '--': 'minor',
        'critical': 'critical',
        'high': 'major',
        'normal': 'minor',
        'low': 'trivial',
        'wish': 'trivial',
    },
    fields={
        "cc": "watchers",
        "owner": "assignee",
        "component": "component",
        "milestone": "milestone",
        "priority": "priority",
        "status": "status",
        "resolution": "status",
        "summary": "title",
        "type": "kind",
        "version": "version",
        "attachment": "attachment"
    },
    authors={
        "guest": None
    }

)

class TracObject(Base):
    __abstract__ = True

    def __init__(self, env, **kw):
        self.env = env
        super(TracObject, self).__init__(**kw)

    def to_json(self):
        return {"name": self.name}

    def insert_in_issuedb(self, issuedb):
        issuedb[self.bb_name].append(self.to_json())


class HasAuthor(object):
    @property
    def bb_user(self):
        return self.env['authors'].get(self.author, self.author)


class Milestone(TracObject):
    __tablename__ = "milestone"

    bb_name = "milestones"


class Version(TracObject):
    __tablename__ = "version"

    bb_name = "versions"

class Component(TracObject):
    __tablename__ = "component"

    bb_name = "components"

class AttachmentFile(object):
    def __init__(self, type_, filename, id_):
        self.filename = filename
        self.id = id_
        self.type = type_

    def source_file(self, attachment_path):
        basename, ext = os.path.splitext(self.filename)
        dir_ = hashlib.sha1(self.id.encode('utf-8')).hexdigest()
        source = os.path.join(
                        attachment_path,
                        self.type,
                        dir_[0:3],
                        dir_,
                        hashlib.sha1(self.filename.encode('utf-8')).hexdigest() + ext
                        )
        source = urllib.quote(source)
        return source

    def bb_issue_path(self, attachment_path):
        return os.path.join(attachment_path, str(self.id), self.filename)


class Attachment(HasAuthor, TracObject):
    __tablename__ = "attachment"

    bb_name = "attachments"

    @util.memoized_property
    def file(self):
        return AttachmentFile("ticket", self.filename, self.id)

    def insert_in_issuedb(self, issuedb):
        if self.description:
            c1 = Comment(
                        self.env,
                        time=self.time,
                        author=self.author,
                        newvalue=self.description,
                        ticket=self.id,
                        field="comment",
                    )
            c1.insert_in_issuedb(issuedb)

            t1 = TicketChange(
                            self.env,
                            time=self.time,
                            author=self.author,
                            newvalue=self.filename,
                            ticket=self.id,
                            field="attachment",
                            )
            t1.insert_in_issuedb(issuedb, comment=c1)

        super(Attachment, self).insert_in_issuedb(issuedb)

    def to_json(self):
        return {
            "filename": self.filename,
            "issue": self.id,
            "path": self.file.bb_issue_path("attachments"),
            "user": self.bb_user,
            }

class TicketChange(HasAuthor, TracObject):
    __tablename__ = "ticket_change"
    bb_name = "logs"

    field = Column(String, primary_key=True)

    __mapper_args__ = {"polymorphic_on": case(
                                    whens=[(field == "comment", "comment")],
                                    else_='ticket_change'
                                ),
                        "polymorphic_identity": "ticket_change"}

    def _convert_field(self, value):
        field_conv = {
            "priority": "priorities",
            "status": "statuses",
            "resolution": "statuses",
            "type": "types",
        }

        if self.field in self.env['fields']:
            if self.field in field_conv:
                conv = self.env[field_conv[self.field]]
                value = conv.get(value)
            if value is None:
                value = ""
        return value

    @property
    def bb_changed_from(self):
        return self._convert_field(self.oldvalue)

    @property
    def bb_changed_to(self):
        return self._convert_field(self.newvalue)

    @property
    def bb_field(self):
        if self.field in self.env['fields']:
            return self.env['fields'][self.field]
        else:
            return self.field

    def insert_in_issuedb(self, issuedb, comment=None):

        if self.field != "comment":
            is_supported_field = self.field in self.env['fields']
            if self.field == "status":
                # all other statuses handled by "resolution"
                is_supported_field = self.newvalue in ('reopened',)
            comment_id = None

            if comment is not None:
                comment_id = comment.comment_id

            elif self.ticket_obj is not None:
                for comment in self.ticket_obj.bb_ticket_comments:
                    # search for a Comment with the same timestamp
                    # as this one
                    if comment.time == self.time:
                        comment_id = comment.comment_id

                        comment.is_for_fields.add(is_supported_field)
                        break

            if comment_id is None:
                # create a corresponding "blank" comment
                # for a ticketchange with no comment
                comment = Comment(
                                self.env,
                                time=self.time,
                                newvalue=None,
                                author=self.author,
                                ticket=self.ticket,
                                field="comment",
                                )
                comment.is_for_fields.add(is_supported_field)
                comment_id = comment.comment_id
                comment.insert_in_issuedb(issuedb)
        else:
            # skip comments that are:
            # A. blank, and
            # B. are linked only to TicketChange entries that
            # aren't being included in the export.
            is_supported_field = self.newvalue or \
                        True in self.is_for_fields
            comment_id = self.comment_id
        if is_supported_field:
            issuedb[self.bb_name].append(self.to_json(comment_id))

    def to_json(self, comment_id):
        return {
            "issue": self.ticket,
            "changed_from": self.bb_changed_from,
            "changed_to": self.bb_changed_to,
            "comment": comment_id,
            "field": self.bb_field,
            "created_on": _convert_time(self.time),
            "user": self.bb_user,
            }

class Comment(TicketChange):
    bb_name = "comments"

    __mapper_args__ = {"polymorphic_identity": "comment"}

    @util.memoized_property
    def comment_id(self):
        return next(comment_id)

    @util.memoized_property
    def is_for_fields(self):
        return set()

    def to_json(self, comment_id):
        return {
            "content": _trac_to_markdown(self.newvalue),
            "created_on": _convert_time(self.time),
            "id": comment_id,
            "issue": self.ticket,
            "updated_on": _convert_time(self.time, BB_MINIMUM_UPDATED),
            "user": self.bb_user
            }


comment_id = itertools.count()


class Ticket(TracObject):
    __tablename__ = "ticket"
    bb_name = "issues"

    attachments = relationship("Attachment",
                        primaryjoin="and_(Ticket.id == foreign(cast(Attachment.id, Integer)), "
                                    "Attachment.type == 'ticket')")

    ticket_changes = relationship("TicketChange",
                        primaryjoin="Ticket.id == foreign(TicketChange.ticket)",
                        backref="ticket_obj",
                        order_by="TicketChange.time")


    @property
    def bb_ticket_changes(self):
        return [
            tc for tc in self.ticket_changes if not isinstance(tc, Comment)
        ]

    @property
    def bb_ticket_comments(self):
        comments = [
            tc for tc in self.ticket_changes if isinstance(tc, Comment)
        ]
        return comments

    def to_json(self):
        content_updated_time = object_session(self).query(
                                    func.max(TicketChange.time)
                                ).with_parent(self).scalar()
        if not content_updated_time:
            content_updated_time = self.changetime
        else:
            content_updated_time = max(content_updated_time, self.changetime)

        return {
            "reporter": self.reporter,  # TODO! author translate!
            "assignee": self.owner,     # TODO! author translate!
            "title": self.summary[0:255],
            "content": _trac_to_markdown(self.description),
            "created_on": _convert_time(self.time),
            "updated_on": _convert_time(content_updated_time),
            "content_updated_on": _convert_time(content_updated_time, BB_MINIMUM_UPDATED),
            "edited_on": None,
            "id": self.id,
            "kind": self.env['types'].get(self.type, "task"),
            "component": self.component,
            "milestone": self.milestone,
            "priority": self.env['priorities'][self.priority],
            "status": self.env['statuses'][self.status],
            "version": self.version,
            "watchers": [x.strip() for x in self.cc.split(",")] if self.cc else None,
            }


    def insert_in_issuedb(self, issuedb):
        TracObject.insert_in_issuedb(self, issuedb)
        for a in self.attachments:
            a.insert_in_issuedb(issuedb)
        for tc in self.bb_ticket_changes:
            tc.insert_in_issuedb(issuedb)
        for cmt in self.bb_ticket_comments:
            cmt.insert_in_issuedb(issuedb)



def _build_issue_db(env):
    env['issue_db'] = issue_db = {
        "issues": [],
        "comments": [],
        "attachments": [],
        "logs": [],
        "meta": {
            "default_assignee": None,
            "default_component": None,
            "default_kind": "bug",
            "default_milestone": None,
            "default_version": None
            },
        "components": [],
        "milestones": [],
        "versions": []
    }
    url = env['url']
    engine = create_engine(url)

    Base.prepare(engine, reflect=True)

    from sqlalchemy import event

    @event.listens_for(Base, "load", propagate=True)
    def load(target, context):
        session = context.session
        target.env = session.info['env']

    session = Session(engine, autoflush=False)
    session.info['env'] = env

    for cls in Milestone, Version, Component, Ticket:
        for obj in session.query(cls):
            obj.insert_in_issuedb(issue_db)

    all_log_comment_ids = set([
        rec["comment"] for rec in issue_db["logs"]
    ])
    all_comment_ids = set([
        rec["id"] for rec in issue_db["comments"]
    ])

    assert all_comment_ids.issuperset(all_log_comment_ids), \
            "comments were suppressed that are linked to log records"

    return issue_db

def trac_to_import(url, attachments, output, opts):
    output = output or "trac_to_bitbucket.zip"
    zipf = zipfile.ZipFile(output, "w")

    env_opts = dict(
        (key, value.copy()) for key, value in default_opts.items()
    )
    for key in opts:
        if key in env_opts:
            assert isinstance(opts[key], dict), \
                "Dictionary is required for %s" % key
            env_opts[key].update(opts[key])
        else:
            env_opts[key] = opts[key]
    env_opts['url'] = url
    env_opts['attachments'] = attachments

    idb = _build_issue_db(env_opts)

    found_attachments = []
    for att in idb["attachments"]:
        attachment_file = AttachmentFile("ticket", att['filename'], att['issue'])
        source = attachment_file.source_file(attachments)
        if not os.path.exists(source):
            noext, ext = os.path.splitext(source)
            if not os.path.exists(noext):
                print("Warning: attachment on ticket %s does not exist: %s, %s" %
                        (att['issue'], att['path'], source))
                continue
            else:
                source = noext
        found_attachments.append(att)
        zipf.write(source, att["path"])
    idb["attachments"] = found_attachments
    zipf.writestr("db-1.0.json", json.dumps(idb, indent=8))
    zipf.close()



